#!/bin/sh

GetSentence(){
	# Provide context
        echo "Take away:"

	# Get random sentences
 	curl -s https://www.coolgenerator.com/random-sentence-generator > sentences.txt
	# Clean and select 1
        grep -o "Random sentence.*span>.*span" sentences.txt | sed "s/.*<span>//" | sed "s/..span.*//" | head -n 1 | fmt -g 40

	# Clean-up
        rm sentences.txt

}


GetImages(){
        N_slides=$1 # DEV: default here.

        # Pre-clean-up
	rm tpg_pics/*
        # Do all this away from sight.
	cd tpg_pics
	

        # Scrape random generator for "pixabay" images
	rand_gen_url="https://www.coolgenerator.com/random-image-generator?new"
	for two_dumb in $(seq $(($N_slides/2 + 1)))
        do
		curl -s $rand_gen_url | grep -o "pixabay.com/get/[a-z0-9\_]*...." >> images_list
        done

	# Grab each image. 
        for url in $(head -n $N_slides images_list)
        do
                wget --quiet --https-only $url
        done

	# Clean-up - necessary to avoid "images_list" showing up in presentation
	rm images_list
	
	# Return to main directory
	cd ..
}


presentation_file="tpg_presentation.txt"

# Init presentation
echo "The\nPresentation\nGame\n" > $presentation_file

# Add images to presentation
GetImages 3
ls -1 tpg_pics/ | sed "s/.*/@tpg_pics\/&\n/" >> $presentation_file

# Add "take-away"
GetSentence >> $presentation_file

#
echo "\nFIN" >> $presentation_file

# Run presentation
sent $presentation_file
