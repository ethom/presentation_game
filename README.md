The Presentation Game
=====================

An improv game where actors have give a presentation with slides unseen!

Install
-------
> sudo apt install sent

> git clone https://gitlab.com/ethom/presentation_game

> cd presentation_game

> ./play.sh

Depends
-------
- [**sent**](https://tools.suckless.org/sent/) (available on some package managers)
- curl
- wget
- coreutils



Disclaimer
----------

I grabbing images from a site called "coolgenerator.com", which inturn is grabbing 
images from "pixabay.com" ... so I have no idea what the contents of the images or
sentences will be. 

